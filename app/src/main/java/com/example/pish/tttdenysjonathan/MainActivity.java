package com.example.pish.tttdenysjonathan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * TTT Game class integrating all game functionality
 */
public class MainActivity extends AppCompatActivity {

    //Class variables
    private int move = 0; //Moves count
    private int gameMode = 1; //1 - game against AI, 2 game HumanVsHuman
    private List<ImageButton> squareList = new ArrayList<ImageButton>(); //array of objects
    private int[] gameArray = new int[9]; //array that stores the integer values for nine game boxes
    //(0- empty, 1 - X, 10 - O)

    /**
     * creates an application and sets UI
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetArrayOfGameBlocks();

    }

    /**
     * Puts nine imageBoxes into the list that matches the gameArray.
     * imageBoxes are disabled until the player selects the game mode
     */
    public void SetArrayOfGameBlocks()
    {
        TableLayout rootTableLayout = (TableLayout)findViewById(R.id.gameGrid);
        int count1 = rootTableLayout.getChildCount();
        for (int i = 0; i <= count1; i++) {
            if (rootTableLayout.getChildAt(i) instanceof TableRow) {
                TableRow tr = (TableRow)rootTableLayout.getChildAt(i);
                for (int j=0; j<tr.getChildCount(); j++) {

                    if (tr.getChildAt(j) instanceof ImageButton){
                        ImageButton imBtn = (ImageButton)tr.getChildAt(j);
                        imBtn.setClickable(false);
                        squareList.add(imBtn);
                    }
                }
            }
       }
    }

    /**
     * Method checks whether the game ended determining its outcome and the winner.
     * calls the method showing the pop-dialogue at the end
     */
    public void checkVictoryCondition() {
        String gameRest = "0";


        if (gameArray[0] + gameArray[1] + gameArray[2] == 3 ||
                gameArray[3] + gameArray[4] + gameArray[5] == 3 ||
                gameArray[6] + gameArray[7] + gameArray[8] == 3 ||
                gameArray[0] + gameArray[3] + gameArray[6] == 3 ||
                gameArray[1] + gameArray[4] + gameArray[7] == 3 ||
                gameArray[2] + gameArray[5] + gameArray[8] == 3 ||
                gameArray[0] + gameArray[4] + gameArray[8] == 3 ||
                gameArray[2] + gameArray[4] + gameArray[6] == 3) {
            gameRest = "victory - X";
            showEndGameDialog(gameRest);
         }

        else if (gameArray[0] + gameArray[1] + gameArray[2] == 30 ||
                gameArray[3] + gameArray[4] + gameArray[5] == 30 ||
                gameArray[6] + gameArray[7] + gameArray[8] == 30 ||
                gameArray[0] + gameArray[3] + gameArray[6] == 30 ||
                gameArray[1] + gameArray[4] + gameArray[7] == 30 ||
                gameArray[2] + gameArray[5] + gameArray[8] == 30 ||
                gameArray[0] + gameArray[4] + gameArray[8] == 30 ||
                gameArray[2] + gameArray[4] + gameArray[6] == 30) {
            gameRest = "Victory O";
            showEndGameDialog(gameRest);
        }

        else if (move > 8) {
            gameRest = "draw";

            showEndGameDialog(gameRest);
        }

        //Toast.makeText(MainActivity.this, Integer.toString(move),Toast.LENGTH_LONG).show();
    }

    /**
     * Creates and shows the dialog that informs about the game outcome
      */
    public void showEndGameDialog(String gameRest) {
        int stop;
        AlertDialog.Builder dlgVictory = new AlertDialog.Builder(this);
        dlgVictory.setMessage(gameRest);
        dlgVictory.setTitle("Game Result");
        dlgVictory.setPositiveButton("Ok", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {resetGame(null);}
        });
        dlgVictory.create().show();

    }

    //Should be replaced with new activity according to the specs
    public void showAbout(View view) {
        AlertDialog.Builder dlgAbout = new AlertDialog.Builder(this);
        dlgAbout.setMessage(R.string.about);
        dlgAbout.setTitle(R.string.abouthead);
           dlgAbout.setPositiveButton("Ok", new OnClickListener() {
                       public void onClick(DialogInterface dialog, int which) {}
                   });
        dlgAbout.create().show();
       }

    /**
     * This method is called when user selects the game mode.
     * It starts the game and makes the grid clickable
     */
    public void startGame(View view)
    {
       if (findViewById(R.id.oneplayer) == view)
           gameMode = 1;
        else
           gameMode = 2;
        Button singleButton = (Button)findViewById(R.id.oneplayer);
        singleButton.setVisibility(View.INVISIBLE);
        Button multiButton = (Button)findViewById(R.id.multiplayer);
        multiButton.setVisibility(View.INVISIBLE);

        for (int i=0; i<squareList.size(); i++) {
            squareList.get(i).setClickable(true);
        }
    }

    /**
     * Resets game environment after the game is over
     */
    public void resetGame(View view)
    {
        for (int i=0; i<squareList.size(); i++) {
            squareList.get(i).setClickable(false);
            squareList.get(i).setImageResource(R.drawable.emptysquare);
            gameArray[i] =0;
        }
        move = 0;
        Button singleButton = (Button)findViewById(R.id.oneplayer);
        singleButton.setVisibility(View.VISIBLE);
        Button multiButton = (Button)findViewById(R.id.multiplayer);
        multiButton.setVisibility(View.VISIBLE);

    }

    /**
     * Handles clicks on the grid showing X or O
     */
    public void  tttOnClick(View view)
    {
        if (move%2==0) {
            ((ImageButton) view).setImageResource(R.drawable.cross);
            gameArray[squareList.indexOf(view)] = 1;
        }
        else{
            ((ImageButton) view).setImageResource(R.drawable.ooo);
            gameArray[squareList.indexOf(view)] = 10;
        }

        view.setClickable(false);
        move++;
        checkVictoryCondition();

        if (gameMode==1 && move>0)
            aiMove();
    }

    /**
     * contains AI random move algorithm (AI plays O)
     */
    private void aiMove()
    {
        int random1;
        do{
        random1 = (int)(Math.random()*9);
        }
        while (gameArray[random1] == 1 || gameArray[random1] == 10);

        gameArray[random1]=10;
        squareList.get(random1).setClickable(false);
        squareList.get(random1).setImageResource(R.drawable.ooo);
        move++;
        checkVictoryCondition();
    }

}
